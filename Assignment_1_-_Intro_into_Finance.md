# TITLE
The rise of FinTech and ho it disrubted the financial transactions world!
## Overview and Origin
Square, Inc. is an American financial services, merchant services aggregator, and mobile payment company based in San Francisco, California. The company markets software and hardware payments products and has expanded into small business services. 
The company was founded in 2009 by Jack Dorsey and Jim McKelvey and launched its first app and service in 2010. It has been traded as a public company on the New York Stock Exchange since November 2015 with the ticker symbol SQ.
* Name of company
Square, Inc.
* When was the company incorporated?
2009
* Who are the founders of the company?
Jack Dorsey and Jim McKelvey
* How did the idea for the company (or project) come about?
The idea started with a little white card reader that can be plugged into any 3.5mm audio jack for smart devices as a simple way for artisans and street vendors to accept credit cards. The idea soon revolutionized the payment processing inductry and the Company started diversifying into other finance transaction systems and hardware tools that are considered today as comprehensive solutions for most business sizes.
* How is the company funded? How much funding have they received?
Jim McKelvey after stepping down as Twitter CEO in late 2008 and Jack Dorsey cofounded the company.

## Business Activities:

* What specific financial problem is the company or project trying to solve?
The Company started as Financial transactions and payment processing services provider. It then evolved to include various financial transactions and payment processing solutions and systems that serve all business sizes.
* Who is the company's intended customer?  Is there any information about the market size of this set of customers?
What solution does this company offer that their competitors do not or cannot offer? (What is the unfair advantage they utilize?)
 Unlike conventional payment processing companies, Square has several competitve factors that are summarized as per the following:
	1) There are no monthly fees or set-up costs. The firm claims that its costs are, on average, lower than the costs charged by conventional credit card processors
	2) Swiped payments are deposited directly into a user's bank account within 1-2 business days.
	3) The Company processes all different types of debit and credit cards.
* Which technologies are they currently using, and how are they implementing them? (This may take a little bit of sleuthing–– you may want to search the company’s engineering blog or use sites like Stackshare to find this information.)
As per the Square Stackshare company profile, Square utilizes various applications including:[JavaScript, Java, MySQL, Redis, Amazon EC2, Sass, Ruby...etc] 

The company also utlizes the following utilities as per it's stackshare profile:
	1) Google Maps
	2) Looker
	3) Retrofit
	4) HackerOne

## Landscape:

* What domain of the financial industry is the company in?
Financial Services

* What have been the major trends and innovations of this domain over the last 5-10 years?
Payment processing, security, FinTech
* What are the other major companies in this domain?
	- Moneris
	- Helcim
	- KIS payments
	- Shopify

## Results

* What has been the business impact of this company so far?
	Square has disrubted the financial services industry. Especially when it comes to payment processing and transactions. Square's realy competitive advantage is its' reach to various business siszes in the economy due to its' business model and low costs compared to its' competitors.
* What are some of the core metrics that companies in this domain use to measure success? How is your company performing, based on these metrics?
	Some of the metrics that can be used would be:
	1) Volume of transactions allocated based on geoigraphical area for a certain period. This metric would provide the company with a line of sight for major geographical areas that the company might need to focus on or increase it's presence in.
	2) Transaction balances processed for a certain period 
	3) Volume of transactions and transaction volume allocated based on geoigraphical area for a certain period. the results from this metric can also be compared to the results of the metrics 1 and 2 mentioned above when comparing similar historical points.
	4) Comparatives and financial analytics between transactions volume and balances for a certain period from prior year versus the same period from current year.
* How is your company performing relative to competitors in the same domain?
Square has become a market leader in the financial services world. This was achived due to the continuing expansion of the company's products diversification and expansion since its' establishment in 2009. Square is also very competitve in their pricing plans for their customers which assisted in establishing customer and market trust in the company.

## Recommendations

* If you were to advise the company, what products or services would you suggest they offer? (This could be something that a competitor offers, or use your imagination!)
I would look more into the internet of things as I think such companies would start intergrating their solutions as they become bigger and more experienced. Another area of enhancement that Square has already started developing is crypto payment processing which should open unlimited opportunities if implemented innovatively.
* Why do you think that offering this product or service would benefit the company?
It would ease the company's reach to more consumer base which would ultimately drive businesses to contract with Square.
* What technologies would this additional product or service utilize?
Internet of Things
* Why are these technologies appropriate for your solution?
I think the world is starting to be driven by the interenet of things. There is also a big driven appetite from busniesses and consumers to find solutions for the current world crises. Most of these solutions would eventually need a payment processor, like Square, to facilitate their businesses.


Resources used:
https://squareup.com/
https://stackshare.io
https://www.investopedia.com/articles/tech/021017/square.asp
https://en.wikipedia.org/wiki/Square,_Inc.

